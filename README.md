# nesting AMI build for Apple Silicon MacOS SaaS Runners

The `nesting` AMI is used by GitLab-Runner's Instance autoscaler and provides a VM that includes:

- [nesting](https://gitlab.com/gitlab-org/fleeting/nesting) 
- [Tart](https://github.com/cirruslabs/tart/) hypervisor (nesting uses Tart to create VMs on Apple Silicon)
- Apple Silicon VM images, as built by this repository with Packer and Tart.

This allows GitLab-Runner to create isolated environments for MacOS jobs on top of AWS's MacOS instances.
