#!/bin/bash

set -euo pipefail

set -x

echo "Checking AWS_REGION variable"
: "${AWS_REGION:?AWS_REGION environment variable is not set. Please set it before running the script.}"
echo "Checking AMIS_TO_DELETE variable"
: "${AMIS_TO_DELETE:?AMIS_TO_DELETE environment variable is not set. Please set it to a space-separated list of AMI IDs.}"

echo "Deleting AMIs"

# Iterate over each AMI ID
for imageId in $AMIS_TO_DELETE; do
  echo "Deleting image $imageId..."

  snapshotId=$(aws --region "$AWS_REGION" ec2 describe-images --image-ids "$imageId" --query 'Images[0].BlockDeviceMappings[0].Ebs.SnapshotId' --output text)

  echo "Deregistering AMI $imageId"
  aws --region "$AWS_REGION" ec2 deregister-image --image-id "$imageId"

  echo "Deleting snapshot $snapshotId"
  aws --region "$AWS_REGION" ec2 delete-snapshot --snapshot-id "$snapshotId"
done