#!/bin/zsh

# download image tar.zst archive from S3
# used in nesting-start.sh

set -euo pipefail

info() {
  echo -e "\033[32m[$(date -Iseconds)] ${*}\033[0m"
}

info "Starting download $1 as $2"

sudo mkdir -p "$2"
sudo chown -R ec2-user:staff "$2"

/usr/local/bin/s3pipe "$1" | zstd -d | tar -xf - -C "$2"

info "Finished download $1 as $2"
