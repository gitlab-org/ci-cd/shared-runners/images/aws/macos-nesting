#!/bin/zsh

set -euo pipefail

error() {
  echo -e "\033[31m[$(date -Iseconds)] ${*}\033[0m"
}

warning() {
  echo -e "\033[33;1m[$(date -Iseconds)] ${*}\033[0m"
}

info() {
  echo -e "\033[32m[$(date -Iseconds)] ${*}\033[0m"
}

echo
echo "=============================================="
warning "nesting initialization script starts here!"

info "disable spotlight"
sudo mdutil -a -i off || warning "nothing to disable"

info "disable com.apple.peopled"
sudo launchctl stop com.apple.peopled || warning "nothing to disable"

info "disable gatekeeper"
sudo spctl --global-disable || warning "nothing to disable"

info "'pause' executables that we don't need but are unable to disable/kill"

info "- kill wifiFirmwareLoader"
sudo kill -STOP "$(ps aux | grep wifiFirmwareLoader | grep -v grep | awk '{print $2}')" || warning "didn't pause"

info "- kill airportd"
sudo kill -STOP "$(pidof airportd)" || warning "didn't pause"

info "- kill bluetoothd"
sudo kill -STOP "$(pidof bluetoothd)" || warning "didn't pause"

info "Checking NESTING_SOCKET variable"
if [[ -z "${NESTING_SOCKET}" ]]; then
    error "NESTING_SOCKET environment variable is not set or does not contain a valid path to a socket file. Exiting."
    exit 1
fi

# If a socket file from a previous serve was not cleaned up, delete it
info "Deleting previously existing socket"
if [[ -e "${NESTING_SOCKET}" ]]; then
    warning "Socket file exists at path: ${NESTING_SOCKET}"
    info "Deleting the previous socket file..."
    rm "${NESTING_SOCKET}"
    info "Previous socket file deleted."
else
    info "No previous socket file exists at path: ${NESTING_SOCKET}"
fi

# Wait for ec2-macos-init to finish intialization so
# any user scripts will have been executed.
info "Waiting for ec2-macos-init to finish"
set +e
# There are no "structured" APIs via launchctl that provide this information.
# launchctl list will report a zero exit code even if the service never ran.
# `print` at least will say "last exit code = (never exited)" in that case.
until launchctl print system/com.amazon.ec2.macos-init | grep -q "last exit code = 0"; do sleep 1; done
set -e

# Use the nesting.json to create working directory if necessary.
# The user script may have edited this file.
nesting_dir="$(jq -r '.working_directory' /Users/ec2-user/nesting.json)"
if [[ -z "${nesting_dir}" ]]; then
  error "working_directory is not set in nesting.json"
  exit 1
fi

images_dir="$(jq -r '.image_directory' /Users/ec2-user/nesting.json)"
if [[ -z "${images_dir}" ]]; then
  error "image_directory is not set in nesting.json"
  exit 1
fi

info "Create ${nesting_dir} directory"
sudo mkdir -p "${nesting_dir}"
sudo chown -R ec2-user:staff "${nesting_dir}"

info "Create ${images_dir} directory"
sudo mkdir -p "${images_dir}"
sudo chown -R ec2-user:staff "${images_dir}"

# When in VF mode - download the images from S3 to the image directory.
# We can make this the default operation when tart is removed.
if [[ "${NESTING_HYPERVISOR}" == "virtualizationframework" ]]; then
  info "Downloading images to ${images_dir}"
  parallel --arg-file /Users/ec2-user/images.list -t --colsep ' ' --lb --halt-on-error "now,fail=1" /Users/ec2-user/download_image.sh {1} "${images_dir}/"{2}
fi

# On M2 Pro we have more CPU and Memory available so we can increase the limits
# given for the nested VMs
info "Check if on M2 Pro"
if system_profiler SPHardwareDataType | grep -q "Chip: Apple M2 Pro"; then
  info "[M2 Pro] Updating images specs"
  for i in "${images_dir}"/*; do
    img=$(basename "${i}")
    info "[M2 Pro] Updating ${img}"

    jq '.memorySize = 17179869184 | .memorySizeMin = 8589934592 | .cpuCount = 6 | .cpuCountMin = 3' < "${i}/config.json" > "${i}/config.json.new"
    mv "${i}/config.json" "${i}/config.json.old"
    mv "${i}/config.json.new" "${i}/config.json"
  done
fi

info "Start nesting"
exec /usr/local/bin/nesting serve -config /Users/ec2-user/nesting.json -hypervisor "${NESTING_HYPERVISOR}"
