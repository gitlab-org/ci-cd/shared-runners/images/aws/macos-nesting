# defaults targetting macOS arm64 M chips
export GOOS ?= darwin
export GOARCH ?= arm64
export CGO_ENABLED=0

.PHONY: build-s3pipe
build-s3pipe:
	go build -o s3pipe ./cmd/s3pipe

.PHONY: build-vncDriver
build-vncDriver:
	go build -o vncDriver ./cmd/vncDriver

.PHONY: build-all
build-all: build-s3pipe build-vncDriver
