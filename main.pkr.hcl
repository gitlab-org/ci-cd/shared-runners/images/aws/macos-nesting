packer {
  required_version = "~> 1.9"

  required_plugins {
    amazon = {
      version = ">= 1.3.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

#############
# Variables #
#############

variable "build_region" {
  type = string

  description = "AWS region in which to build the image"
}

variable "host_resource_group_arn" {
  type = string

  description = "ARN of the host resource group used to spin-up AWS mac2.metal instances"
}

variable "license_configuration_arn" {
  type = string

  description = "ARN of the license configuration used to spin-up AWS mac2.metal instances"
}

variable "nesting" {
  type = object({
    version  = string
    checksum = string
  })

  description = "Object containing version and checksum values of nesting release to be installed on the created AMI"
}

variable nesting_hypervisor {
  type = string

  description = "Hypervisor to be used by nesting"
}

variable "tart_images" {
  type = map(string)

  description = "Hash containing aliases and ECR URLs for Tart images to be installed on the created AMI"
}

variable "compress_images" {
  type    = bool
  default = false

  description = "Defines whether downloaded images for nested VMs should be compressed or left as they are"
}

variable "manifest_file" {
  type = string

  description = "Name of the file that will hold manifest with details of built AMI"
}

variable "source_ami" {
  type = object({
    filters     = map(string)
    owners      = list(string)
    most_recent = bool
  })

  description = "Amazon AMI data source object used to select the image source"
}

variable "target_ami_family" {
  type = string

  description = "Common part of the name given to the AMI"
}

variable "target_ami_version_family" {
  type = string

  description = "Family of the AMI version"
}

variable "target_ami_version" {
  type = string

  description = "Version of the AMI"
}

variable "tart_download_pat" {
  type = string

  description = "PAT for accessing tart binaries in the generic repo"
}

variable "gitlab_pipeline_id" {
  type    = number
  default = 0
}

variable "gitlab_pipeline_url" {
  type    = string
  default = ""
}

variable "gitlab_job_id" {
  type    = number
  default = 0
}

variable "gitlab_job_url" {
  type    = string
  default = ""
}

variable "gitlab_commit_ref_name" {
  type    = string
  default = ""
}

variable "gitlab_project_path" {
  type    = string
  default = ""
}

variable "gitlab_commit_sha" {
  type    = string
  default = ""
}

#########################
# AMI source definition #
#########################

locals {
  ami_name = format("%s-arm64-%s-%s", var.target_ami_family, var.target_ami_version, formatdate("YYYYMMDDhhmmssZ", timestamp()))
  # temporary workaround for virtualizationframework where we don't want 
  # the tart images to be used directly.
  tart_images = var.nesting_hypervisor == "tart" ? var.tart_images : {}
}

data "amazon-ami" "source" {
  filters     = var.source_ami.filters
  owners      = var.source_ami.owners
  most_recent = var.source_ami.most_recent

  region = var.build_region
}

source "amazon-ebs" "macos" {
  ami_name = local.ami_name

  instance_type = "mac2.metal"
  region        = var.build_region

  ena_support   = true
  ebs_optimized = true

  temporary_security_group_source_public_ip = true

  ssh_username = "ec2-user"
  // AWS says time to readiness can be up to 40min for M1 instances,
  // 15min for Intel. https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-mac-instances.html
  ssh_timeout = "1h"

  launch_block_device_mappings {
    device_name = "/dev/sda1"

    delete_on_termination = true

    volume_type = "gp3"
    # For tart each macOS image is around 50GB when uncompressed.
    # For VF with secondary disk we can have a smaller image.
    volume_size = var.nesting_hypervisor == "tart" ? 500 : 100
    iops        = 3000
    throughput  = 750
  }

  placement {
    host_resource_group_arn = var.host_resource_group_arn
    tenancy                 = "host"
  }

  license_specifications {
    license_configuration_request {
      license_configuration_arn = var.license_configuration_arn
    }
  }

  source_ami = data.amazon-ami.source.id

  run_tags = {
    Name          = local.ami_name
    Family        = var.target_ami_family
    Version       = var.target_ami_version
    VersionFamily = var.target_ami_version_family
  }

  run_volume_tags = {
    Name          = local.ami_name
    Family        = var.target_ami_family
    Version       = var.target_ami_version
    VersionFamily = var.target_ami_version_family
  }

  tags = {
    GitlabPipelineID    = var.gitlab_pipeline_id
    GitlabPipelineURL   = var.gitlab_pipeline_url
    GitlabJobID         = var.gitlab_job_id
    GitlabJobURL        = var.gitlab_job_url
    GitlabCommitRefName = var.gitlab_commit_ref_name
    GitlabProjectPath   = var.gitlab_project_path
    GitlabCommitSHA     = var.gitlab_commit_sha
  }

  temporary_iam_instance_profile_policy_document {
    Version = "2012-10-17"
    Statement {
      Action = [
        "ecr:GetDownloadUrlForLayer",
        "ecr:BatchGetImage",
        "ecr:CompleteLayerUpload",
        "ecr:BatchCheckLayerAvailability",
        "ecr:GetAuthorizationToken"
      ]
      Effect   = "Allow"
      Resource = ["*"]
    }
  }

  // It takes a while for the created image to settle down from `Pending` to `Available`.
  // It seems that with default settings for amazon ebs builder is not enough and packer
  // build call ends randomly with:
  //
  // Error waiting for AMI: Failed with ResourceNotReady error, which can have a variety of causes.
  //   For help troubleshooting, check our docs: https://www.packer.io/docs/builders/amazon.html#resourcenotready-error
  //   original error: ResourceNotReady: exceeded wait attempts
  //
  // Looking at the logs and creation time attached to the image, increasing the waiting
  // time from 10 minutes (40 * 15sec) to 150 minutes (150 * 60sec) will hopefully resolve
  // this problem.
  aws_polling {
    max_attempts  = 150
    delay_seconds = 60
  }
}

###############################
# AMI provisioning definition #
###############################

build {
  name = "fleeting"

  sources = [
    "source.amazon-ebs.macos"
  ]

  provisioner "file" {
    source      = "./assets/"
    destination = "/Users/ec2-user/"
  }

  provisioner "file" {
    destination = "/Users/ec2-user/images.list"
    content     = <<EOT
%{for name, image in local.tart_images~}
${image} ${name}
%{endfor~}
EOT
  }

  provisioner "file" {
    destination = "/Users/ec2-user/nesting.checksum"
    content     = "${var.nesting.checksum}  nesting"
  }

  provisioner "file" {
    source      = "./scripts/clone_image.sh"
    destination = "/Users/ec2-user/clone_image.sh"
  }

  provisioner "shell-local" {
    inline = [
      "make build-all"
    ]
  }

  provisioner "file" {
    sources     = ["./s3pipe", "./vncDriver"]
    destination = "/Users/ec2-user/"
    generated   = true
  }

  provisioner "shell" {
    scripts = concat([
      "./scripts/01_base_system_preparation.sh",
      "./scripts/20_install_nesting.sh",
      "./scripts/21_install_s3pipe.sh",
      ],
      # temporary condition to remove time consuming tart stuff
      # if we are building a VF image
      var.nesting_hypervisor == "tart" ? [
        "./scripts/02_docker_credentials_helper.sh",
        "./scripts/22_install_tart.sh",
        "./scripts/31_prepare_vm_images.sh",
        ] : [
        "./scripts/30_nesting_full_disk_access.sh",
      ],
      [
        "./scripts/99_cleanup.sh",
    ])

    env = {
      NESTING_VERSION    = var.nesting.version
      NESTING_HYPERVISOR = var.nesting_hypervisor
      COMPRESS_IMAGES    = var.compress_images ? "yes" : "no"
      TART_DOWNLOAD_PAT  = var.tart_download_pat
    }
  }

  post-processor "manifest" {
    output     = "manifest.json"
    strip_path = true
  }

  # remove attached license and copy to publish region
  post-processor "shell-local" {
    env = {
      BUILD_LICENSE_CONFIGURATION_ARN = var.license_configuration_arn
      AMI_NAME                        = local.ami_name
    }

    inline = [
      "make remove_ami_license_spec",
      "make copy_ami_to_publish_region"
    ]
  }
}
