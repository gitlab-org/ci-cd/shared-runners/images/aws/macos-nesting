nesting = {
  version  = "v0.2.1",
  checksum = "0b113f379c918f963163b1da800797a9fda32d8959b0609b1ef6d465aa3a2050",
}

tart_images = {
  "macos-13-xcode-14" = "915502504722.dkr.ecr.us-east-1.amazonaws.com/macos-13-xcode-14@sha256:c327769e88c2c9233c5e3be1810ae04b734b5430902dd3cc946874056fdef773",
  "macos-14-xcode-15" = "915502504722.dkr.ecr.us-east-1.amazonaws.com/macos-14-xcode-15@sha256:82ccc1911e2d60166c8c12281a35803bbd2f9e8ee3c099a0b5fdb6523076eaa9",
  "macos-15-xcode-16" = "915502504722.dkr.ecr.us-east-1.amazonaws.com/macos-15-xcode-16@sha256:30266e3263c19b6be1cb30c3a50738fffe7e48cfa71f221b6129c692a8bb5795",
}

source_ami = {
  filters = {
    name                = "amzn-ec2-macos-14.6.1-*"
    architecture        = "arm64_mac"
    virtualization-type = "hvm"
    root-device-type    = "ebs"
  }
  owners      = ["amazon"]
  most_recent = true
}
