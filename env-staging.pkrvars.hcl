# https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/main/environments/ci/macos.tf
host_resource_group_arn   = "arn:aws:resource-groups:us-east-2:251165465090:group/macos-image-build"
license_configuration_arn = "arn:aws:license-manager:us-east-2:251165465090:license-configuration:lic-0ac889e8b87fdd78f5ce5f8993fc84b3"
