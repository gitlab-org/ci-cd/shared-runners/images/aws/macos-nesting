#!/bin/zsh
set -euxo pipefail

echo "Cloning $1 as $2"
tart clone $1 $2

if [ "${COMPRESS_IMAGES:-}" = "yes" ]; then
  cd "/Users/ec2-user/.tart/vms/$2"

  echo "Compressing $2"
  tar --totals --zstd -cvvf archive.tar.zst disk.img nvram.bin

  rm disk.img
  rm nvram.bin
fi
