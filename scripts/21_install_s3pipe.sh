#!/bin/zsh

set -euo pipefail

# Load zshrc (we may be in a non-login, non-interactive shell here, since this
# is launched by packer as an executable shell script)

# shellcheck disable=SC1091
source /etc/zprofile
# shellcheck disable=SC1090
source ~/.zshrc

set -x

# Install s3pipe tool
echo "Installing s3pipe"

sudo chmod +x /Users/ec2-user/s3pipe
sudo chown 0:0 /Users/ec2-user/s3pipe
sudo mv /Users/ec2-user/s3pipe /usr/local/bin/
