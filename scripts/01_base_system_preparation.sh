#!/bin/zsh

set -euo pipefail

# Load zshrc (we may be in a non-login, non-interactive shell here, since this
# is launched by packer as an executable shell script)

# shellcheck disable=SC1091
source /etc/zprofile
# shellcheck disable=SC1090
source ~/.zshrc

set -x

sudo diskutil apfs list
CONTAINER_ID=$(diskutil list physical external | awk '/Apple_APFS/ {print $7}')
DISK_ID=$(echo "${CONTAINER_ID}" | cut -d's' -f1-2)

# Resize disk. New disk size is only visible after reboot
echo 'y' | sudo diskutil repairDisk "${DISK_ID}"
sudo diskutil apfs resizeContainer "${CONTAINER_ID}" 0 || echo ""
sudo diskutil apfs list

# Setup language
echo export LANG=en_US.UTF-8 >> ~/.zshrc
echo export LC_ALL=en_US.UTF-8 >> ~/.zshrc

# Enable keyboard navigation used later for nesting full disk access
defaults write NSGlobalDomain AppleKeyboardUIMode -int 3

# Disable spotlight
sudo mdutil -a -i off

# Disable gatekeeper
sudo spctl --global-disable

# Update brew
brew update

# Install coreutils, pidof
brew install coreutils pidof zstd

# Install jq
brew install jq

# Install parallel
brew install parallel
