#!/bin/zsh

set -euo pipefail

# Load zshrc (we may be in a non-login, non-interactive shell here, since this
# is launched by packer as an executable shell script)

# shellcheck disable=SC1091
source /etc/zprofile
# shellcheck disable=SC1090
source ~/.zshrc

set -x

# Install tart
sudo curl -H "Private-Token: ${TART_DOWNLOAD_PAT}" https://gitlab.com/api/v4/projects/51730519/packages/generic/tart/0.36.3/tart -o /usr/local/bin/tart
sudo chmod +x /usr/local/bin/tart

sudo curl -H "Private-Token: ${TART_DOWNLOAD_PAT}" https://gitlab.com/api/v4/projects/51730519/packages/generic/softnet/0.6.1/softnet -o /usr/local/bin/softnet
sudo chmod +x /usr/local/bin/softnet

tart --version
tart list
