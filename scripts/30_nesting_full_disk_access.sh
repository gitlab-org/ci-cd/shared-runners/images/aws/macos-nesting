#!/bin/zsh

set -euo pipefail

# Load zshrc (we may be in a non-login, non-interactive shell here, since this
# is launched by packer as an executable shell script)

# shellcheck disable=SC1091
source /etc/zprofile
# shellcheck disable=SC1090
source ~/.zshrc

set -x

# Enable nesting full disk access
echo "Enabling nesting full disk access"

# Needed if nesting wants to use external EBS volumes
# See also https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/macos-nesting/-/issues/12#note_2157731432
# This applies to the nesting binary as well as any bash or zsh scripts.

# The only way to do this in-band here is with the UI.
# We could also consider https://developer.hashicorp.com/packer/integrations/hashicorp/amazon/latest/components/builder/ebssurrogate
# to replace the TCC.db from another machine
# but this would require multiple image/packer configurations.

# Set a password for ec2-user so we can open the VNC session
# The original password is randomized in ec2-macos-init
# See https://github.com/aws/ec2-macos-init/blob/e247dee90d09488fbb5f76ce443cb01327baf44e/lib/ec2macosinit/usermanagement.go#L174
sudo /usr/bin/dscl . -passwd /Users/ec2-user gitlab

# Allow simple VNC Authentication
# See https://github.com/rfbproto/rfbproto/blob/master/rfbproto.rst#vnc-authentication
# By default macos screen sharing uses Diffie-Hellman based auth
# but we don't have a client that supports this.
sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -configure -clientopts -setvnclegacy -vnclegacy yes -setvncpw -vncpw gitlab

# Start screen sharing
sudo launchctl enable system/com.apple.screensharing
sudo launchctl load -w /System/Library/LaunchDaemons/com.apple.screensharing.plist
sudo launchctl kickstart system/com.apple.screensharing

# Ensure the VNC server is running
nc -vnz 127.0.0.1 5900

# List of programs that require full disk access.
# Note that we omit the root / to make string building easier.
progs=(
	"bin/bash"
	"bin/zsh"
	"usr/local/bin/nesting"
)

# VNC driver loop to add full disk access for each program.
# printf repeats the pattern for each array item.
progCmds="$(printf '<wait5>/<wait>%s<enter><wait><enter><wait5><spacebar>' "${progs[@]}")"

# Command details:
# login, first UI login takes a while to be ready
# open privacy & security via open command
# select full disk access (that's 14 tabs)
# tab navigation is weird here, seems stuck in parent menu, so...
# trigger search
# slowly tab into the main window options
# select + option and enter password
# use / in file open dialog enter program path
# enter and wait for dialog to close
# repeat for each program
/Users/ec2-user/vncDriver -pass gitlab <<- EOF
	<wait><tab>gitlab<enter>
	<wait60s><exec(/usr/bin/open /System/Library/PreferencePanes/Security.prefPane)>
	<wait10><tab><wait><tab><wait><tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><spacebar>
	<wait5><leftAltOn>f<leftAltOff>
	<wait5><tab><wait><tab><wait><tab><wait><spacebar>
	<wait>gitlab<enter>
	${progCmds}
EOF

checkTCC() {
  tccService="$(sqlite3 /Library/Application\ Support/com.apple.TCC/TCC.db "select service from access where client = '${1}';")"
  if [[ $tccService != kTCCServiceSystemPolicyAllFiles ]]; then
	echo "Expected ${1} to be in TCC.db"
	exit 1
  fi
}

# check TCC db record exists for each program
for p in ${progs[@]}; do
  checkTCC "/${p}"
done

# Disable simple VNC Authentication
sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -configure -clientopts -setvnclegacy -vnclegacy no

# Disable screen sharing
sudo launchctl bootout system/com.apple.screensharing
sudo launchctl disable system/com.apple.screensharing

# Reset ec2-user password to something random
set +x
sudo /usr/bin/dscl . -passwd /Users/ec2-user gitlab "$(openssl rand -base64 25)"

set -x

# Prevent ec2-macos-init from trying to reset this password again
# otherwise it'll fail and block SSH key configuration
sudo sed -i .orig 's/RandomizePassword = true/RandomizePassword = false/' /usr/local/aws/ec2-macos-init/init.toml
sudo diff -y --suppress-common-lines /usr/local/aws/ec2-macos-init/init.toml /usr/local/aws/ec2-macos-init/init.toml.orig || :
