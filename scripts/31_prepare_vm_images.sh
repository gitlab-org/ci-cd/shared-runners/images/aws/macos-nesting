#!/bin/zsh

set -euo pipefail

# Load zshrc (we may be in a non-login, non-interactive shell here, since this
# is launched by packer as an executable shell script)

# shellcheck disable=SC1091
source /etc/zprofile
# shellcheck disable=SC1090
source ~/.zshrc

set -x

# Clone VM images from ECR
echo "Cloning base images"
parallel --arg-file /Users/ec2-user/images.list --verbose --colsep ' ' --halt-on-error "now,fail=1" --keep-order --line-buffer /Users/ec2-user/clone_image.sh {1} {2}
