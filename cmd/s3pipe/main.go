package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

const (
	defaultChunksize    int64  = 16 * 1024 * 1024
	defaultConnections  int    = 32
	defaultBucketRegion string = "us-east-1"
)

type downloadRequest struct {
	bucket       string
	bucketRegion string
	key          string
	chunksize    int64
	connections  int
}

func download(ctx context.Context, req downloadRequest, w io.Writer) error {
	cfg, err := config.LoadDefaultConfig(ctx, config.WithRegion(req.bucketRegion))
	if err != nil {
		return fmt.Errorf("loading config: %w", err)
	}

	client := s3.NewFromConfig(cfg)

	head, err := client.HeadObject(ctx, &s3.HeadObjectInput{
		Bucket: aws.String(req.bucket),
		Key:    aws.String(req.key),
	})
	if err != nil {
		return fmt.Errorf("head object: %w", err)
	}

	type chunk struct {
		buf []byte
		n   int
		err error
	}

	ordered := make(chan chan *chunk, req.connections)
	total := aws.ToInt64(head.ContentLength)

	chunkpool := sync.Pool{
		New: func() any {
			return &chunk{buf: make([]byte, req.chunksize)}
		},
	}

	go func() {
		defer close(ordered)

		for off := int64(0); off < total; {
			if ctx.Err() != nil {
				break
			}

			end := min(off+req.chunksize, total) - 1

			item := make(chan *chunk, 1)
			ordered <- item
			chk := chunkpool.Get().(*chunk)
			chk.n = 0
			chk.err = nil

			go func(off, end int64, chunk *chunk) {
				object, err := client.GetObject(ctx, &s3.GetObjectInput{
					Bucket: aws.String(req.bucket),
					Key:    aws.String(req.key),
					Range:  aws.String(fmt.Sprintf("bytes=%d-%d", off, end)),
				})

				if err != nil {
					chunk.err = err
				} else {
					chunk.n, chunk.err = io.ReadAtLeast(object.Body, chunk.buf, int(end-off+1))
					object.Body.Close()
				}

				item <- chunk
			}(off, end, chk)

			off = end + 1
		}
	}()

	for item := range ordered {
		if ctx.Err() != nil {
			break
		}

		chunk := <-item
		if chunk.err != nil {
			return chunk.err
		}

		_, err := w.Write(chunk.buf[:chunk.n])
		if err != nil {
			return err
		}

		chunkpool.Put(chunk)
	}

	return nil
}

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancel()

	var (
		chunksize    int64
		connections  int
		bucketRegion string
	)

	flag.Int64Var(&chunksize, "chunksize", defaultChunksize, "chunk size")
	flag.IntVar(&connections, "connections", defaultConnections, "connections")
	flag.StringVar(&bucketRegion, "bucket-region", defaultBucketRegion, "bucket region")
	flag.Parse()

	if len(flag.Args()) < 1 {
		fmt.Fprintf(os.Stderr, "usage: %s s3://bucket/key\n", os.Args[0])
		return
	}

	u, err := url.Parse(flag.Arg(0))
	if err != nil {
		panic(err)
	}

	if err := download(ctx, downloadRequest{
		bucket:       u.Host,
		bucketRegion: bucketRegion,
		key:          strings.TrimPrefix(u.Path, "/"),
		chunksize:    chunksize,
		connections:  connections,
	}, os.Stdout); err != nil {
		panic(err)
	}
}
