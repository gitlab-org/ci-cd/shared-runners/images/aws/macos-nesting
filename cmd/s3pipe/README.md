# s3pipe

Copied from [kneeslap](https://github.com/saracen/kneeslap).

`s3pipe` downloads an S3 object in chunks concurrently and outputs the data
in order to `stdout`.

## Why

Using `aws s3 cp` to `stdout` was slower than when writing to disk, presumably
because when writing to a file, the download is chunked and parts written
concurrently.

The Go [AWS download manager SDK](https://github.com/aws/aws-sdk-go-v2/tree/main/feature/s3/manager)
is a similarly fast alternative but it only supports the [io.WriterAt](https://pkg.go.dev/io#WriterAt)
interface. Implementing this requires ordering chunks in memory based only on chunk size
and offsets so it is more complex.

We need to download compressed disk images quickly and pipe them to zstd
for decompression.

For example, you could upload an object with:

```shell
tar -cvf - disk.img nvram.bin | zstd --sparse -c -3 -T0 | aws s3 cp - s3://bucket/key.zstd
```

And download it with:

```shell
s3pipe s3://bucket/key | zstd -d | tar -Szxf -
```

This would be comparable to `aws s3 cp s3://bucket/key - | zstd -d | tar -Szxf`,
but the former is much quicker especially on large files.

## Usage

`s3pipe s3://bucket/key` will download and output to `stdout`.

- You can use `pv` to see performance: `s3pipe s3://bucket/key | pv -X`.
- The `-connections` flag sets how many connections are made to the object, default is 32.
- The `-chunksize` flag sets the chunk size, default is 16MB.
