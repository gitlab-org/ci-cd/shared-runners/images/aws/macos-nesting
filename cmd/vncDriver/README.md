# VNC Driver

Used to drive the UI in a VNC session with keyboard commands.

Uses the [Packer plugin bootcommand SDK](https://github.com/hashicorp/packer-plugin-sdk/tree/v0.5.4/bootcommand)
which is typically used for VM startup commands.

We use this specifically to enable full disk access for nesting.

## Usage

```shell
vncDriver -host localhost -port 5900 -pass passwd <<- EOF
    <esc><esc><enter><wait>
    some text here
    <enter>
EOF
```

## VNC Authentication

By default macos screen sharing only supports [Diffie-Hellman key exchange](https://github.com/rfbproto/rfbproto/blob/master/rfbproto.rst#diffie-hellman-authentication).
This uses both a username and password for the authenticated user.
There were no Golang VNC libraries found that supported this auth method and
it's not worth implementing it for this simple use case.

This driver supports [password based VNC Authentication](https://github.com/rfbproto/rfbproto/blob/master/rfbproto.rst#vnc-authentication).
This must be enabled in the macos host explicitly.

## stdin Key Sequences

The special key sequences are documented under the [BootConfig type docs](https://pkg.go.dev/github.com/hashicorp/packer-plugin-sdk@v0.5.4/bootcommand#BootConfig).

Into this we've added a crude `<exec(command args etc)>` escape sequence which will execute (via `os/exec`) the given command
at this point in the key sequence. This runs in the same environment as the VNC driver process. For example:

```shell
    <enter><esc><enter><wait>
    type something here
    <exec(open "program")>
    <wait5><enter>
```

This allows commands to be more easily syncronised with the UI actions.

## Debugging

The best way to do this is with a macos host in AWS.

SSH into the machine. Follow the provisioning steps in [scripts/30_nesting_full_disk_access.sh](../../scripts/30_nesting_full_disk_access.sh).

Disconnect and SSH again forwarding the VNC port:

```shell
ssh -i "your_private_key.pem" ec2-user@ec2-address -L 5900:localhost:5900
```

Use a VNC viewer to connect to the VNC server, using the ec2-user credentials if necessary.
You should see a login screen.

> Don't interact with the UI with your mouse or keyboard, it may break the automation.

Run the `vncDriver` command with password auth and watch the keyboard commands
in the VNC viewer.
