package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/hashicorp/packer-plugin-sdk/bootcommand"
	"github.com/mitchellh/go-vnc"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancel()

	host := flag.String("host", "localhost", "VNC host")
	port := flag.Int("port", 5900, "VNC port")
	pass := flag.String("pass", "", "VNC password (required)")

	flag.Parse()

	errExit := func(msg any) {
		fmt.Fprintln(os.Stderr, msg)
		os.Exit(1)
	}

	if *pass == "" {
		flag.Usage()
		errExit("password required")
	}

	stat, err := os.Stdin.Stat()
	if err != nil {
		errExit("could not stat stdin")
	}

	if stat.Mode()&os.ModeCharDevice != 0 {
		errExit("please provide stdin to execute")
	}

	input, err := io.ReadAll(os.Stdin)
	if err != nil {
		errExit("reading from stdin failed")
	}

	cmds, err := parseInput(string(input))
	if err != nil {
		errExit(err)
	}

	c, err := vncClient(ctx, *host, strconv.Itoa(*port), *pass)
	if err != nil {
		errExit(err)
	}
	defer c.Close()

	if err := runKeyboardCommands(ctx, c, cmds); err != nil {
		errExit(err)
	}
}

type command interface {
	Do(ctx context.Context, driver bootcommand.BCDriver) error
	Validate() []error
}

type execCommand struct {
	cmd string
}

func (c execCommand) Do(ctx context.Context, driver bootcommand.BCDriver) error {
	cs := strings.Fields(c.cmd)

	cmd := exec.CommandContext(ctx, cs[0], cs[1:]...)
	return cmd.Run()
}

func (c execCommand) Validate() []error {
	if len(strings.Fields(c.cmd)) == 0 {
		return []error{errors.New("empty command")}
	}
	return nil
}

func parseInput(input string) ([]command, error) {
	// first extract all exec actions
	r := regexp.MustCompile(`<exec\((.*)\)>`)
	seqs := r.Split(input, -1)
	execs := r.FindAllStringSubmatch(input, -1)

	cmds := []command{}

	for i, s := range seqs {
		if s != "" {
			seq, err := bootcommand.GenerateExpressionSequence(s)
			if err != nil {
				return nil, fmt.Errorf("generating expression sequence: %w", err)
			}
			cmds = append(cmds, seq)
		}

		if i < len(execs) {
			cmds = append(cmds, execCommand{execs[i][1]})
		}
	}

	vErrs := []error{}
	for i, c := range cmds {
		if errs := c.Validate(); len(errs) > 0 {
			vErrs = append(vErrs, fmt.Errorf("command validate[%d]: %w", i, errors.Join(errs...)))
		}
	}
	if len(vErrs) > 0 {
		return nil, errors.Join(vErrs...)
	}

	return cmds, nil
}

func runKeyboardCommands(ctx context.Context, c *vnc.ClientConn, cmds []command) error {
	// turn off verbose key logging
	log.SetOutput(io.Discard)

	dr := bootcommand.NewVNCDriver(c, time.Microsecond*100)

	for _, cmd := range cmds {
		if err := cmd.Do(ctx, dr); err != nil {
			return fmt.Errorf("running command: %w", err)
		}
	}

	return nil
}

func vncClient(ctx context.Context, host, port, pass string) (*vnc.ClientConn, error) {
	d := net.Dialer{
		Timeout: time.Second * 5,
	}
	conn, err := d.DialContext(ctx, "tcp", fmt.Sprintf("%s:%s", host, port))
	if err != nil {
		return nil, fmt.Errorf("dialing vnc port: %w", err)
	}

	c, err := vnc.Client(conn, &vnc.ClientConfig{
		Auth: []vnc.ClientAuth{
			&vnc.PasswordAuth{Password: pass},
		},
	})
	if err != nil {
		return nil, fmt.Errorf("creating vnc client: %w", err)
	}

	return c, nil
}
