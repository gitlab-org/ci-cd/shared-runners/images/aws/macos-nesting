.PHONY: shellcheck
shellcheck:
	shellcheck --exclude SC1071 $(shell find scripts -name "*.sh")

include Makefile.*.mk
