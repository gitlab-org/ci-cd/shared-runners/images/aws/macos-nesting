# Build and publish regions can be the same.
# Ensure both regions have arm64_mac machines.
AWS_BUILD_REGION ?= unknown
AWS_PUBLISH_REGION ?= unknown

# Credentials should be set externally!
AWS_ACCESS_KEY_ID ?= unknown
AWS_SECRET_ACCESS_KEY ?= unknown

#
# Images are currently built in our Sandbox account
# A manual job will publish them to other accounts like the one
# where we host our staging or production environments
#
# 251165465090 is the SaaS MacOS Staging account
#
PUBLISH_ACCOUNT_ID ?= 251165465090

# BUILD_ENVIRONMENT defines where the image will be built.
# Local execution will use the verify-runner sandbox account,
# while the CI execution will use the SaaS MacOS Staging runners
# account.
BUILD_ENVIRONMENT ?= sandbox

# BUILD_RELEASE defines which version of the image should be built
BUILD_RELEASE ?= macos-14-tart

# MANIFEST_FILE contains the machine usable output of `packer build`
# execution. It can be used to read the ID of the built AMI.
MANIFEST_FILE ?= manifest.json

# VERSION contains information about the version of image, that should
# be added to the image as a tag. This will allow to filter out
# images, for example for deleting the development ones
VERSION ?= HEAD

# VERSION_FAMILY contains the information about family of image's version.
# The version family is a general bucket of different similar versions like
# "development", "beta" or "stable".
VERSION_FAMILY ?= dev

# TART_DOWNLOAD_PAT contains token that gives access to the binaries of tart
# and softnet
TART_DOWNLOAD_PAT ?= unknonwn

.PHONY: packer-init
packer-init:
	packer init main.pkr.hcl

.PHONY: packer-validate
packer-validate: packer-init
	@packer validate \
		-var-file main.pkrvars.hcl \
		-var-file env-$(BUILD_ENVIRONMENT).pkrvars.hcl \
		-var-file release-$(BUILD_RELEASE).pkrvars.hcl \
		-var "manifest_file=$(MANIFEST_FILE)" \
		-var "target_ami_version_family=$(VERSION_FAMILY)" \
		-var "target_ami_version=$(VERSION)" \
		-var "tart_download_pat=$(TART_DOWNLOAD_PAT)" \
		-var "build_region=$(AWS_BUILD_REGION)" \
		main.pkr.hcl

.PHONY: packer-fmt
packer-fmt: packer-init
	packer fmt main.pkr.hcl
	git --no-pager diff --compact-summary --exit-code -- main.pkr.hcl

# Manifest file with can have multiple regions (build and publish).
# artifact_id can be e.g. "us-east-1:ami-038c1e42fb7e70014,us-east-2:ami-0ebf2fd535065b2c4"
define jq_manifest_file_ami_id
jq --arg region "$(1)" -r \
'.builds[-1].artifact_id | split(",") | map(split(":")) | map({(.[0]): .[1]}) | add | .[$$region]' "$(MANIFEST_FILE)"
endef

jq_manifest_build_ami_id = $(call jq_manifest_file_ami_id,$(AWS_BUILD_REGION))
jq_manifest_publish_ami_id = $(call jq_manifest_file_ami_id,$(AWS_PUBLISH_REGION))

test_ami:
	echo "$$($(jq_manifest_build_ami_id))"

.PHONY: packer-build
packer-build: packer-init .check_credentials
	@packer build \
		-timestamp-ui \
		-var-file main.pkrvars.hcl \
		-var-file env-$(BUILD_ENVIRONMENT).pkrvars.hcl \
		-var-file release-$(BUILD_RELEASE).pkrvars.hcl \
		-var "manifest_file=$(MANIFEST_FILE)" \
		-var "target_ami_version_family=$(VERSION_FAMILY)" \
		-var "target_ami_version=$(VERSION)" \
		-var "tart_download_pat=$(TART_DOWNLOAD_PAT)" \
		-var "gitlab_pipeline_id=$(CI_PIPELINE_ID)" \
		-var "gitlab_pipeline_url=$(CI_PIPELINE_URL)" \
		-var "gitlab_job_id=$(CI_JOB_ID)" \
		-var "gitlab_job_url=$(CI_JOB_URL)" \
		-var "gitlab_project_path=$(CI_PROJECT_PATH)" \
		-var "gitlab_commit_ref_name=$(CI_COMMIT_REF_NAME)" \
		-var "gitlab_commit_sha=$(CI_COMMIT_SHA)" \
		-var "build_region=$(AWS_BUILD_REGION)" \
		main.pkr.hcl
	@echo "========================================================================"
	@echo " Created image in $(AWS_PUBLISH_REGION) with AMI ID: $$($(jq_manifest_publish_ami_id))"
	@echo "========================================================================"

# AMIs copied between regions cannot use the same license - the license manager
# prevents them from starting. There is no way to remove this license from the
# copied AMI.
# Remove the assigned license from the AMI in the build region.
# This is called from the packer build as a post-processor
# which will set BUILD_LICENSE_CONFIGURATION_ARN.
.PHONY: remove_ami_license_spec
remove_ami_license_spec: AMI_ID ?= $(shell $(jq_manifest_build_ami_id))
remove_ami_license_spec: BUILD_LICENSE_CONFIGURATION_ARN ?= unknown
remove_ami_license_spec: .check_credentials
	@echo "Removing AMI license $(BUILD_LICENSE_CONFIGURATION_ARN)"
	aws --region "$(AWS_BUILD_REGION)" license-manager update-license-specifications-for-resource \
		--resource-arn "arn:aws:ec2:$(AWS_BUILD_REGION)::image/$(AMI_ID)" \
		--remove-license-specifications "LicenseConfigurationArn=$(BUILD_LICENSE_CONFIGURATION_ARN)" \

# Copy AMI from build region to publish region.
# If they are the same this is a no-op.
# Adds the new AMI ID to the manifest file.
# This is called from the packer build as a post-processor
# which will set AMI_NAME.
.PHONY: copy_ami_to_publish_region
ifeq ($(AWS_BUILD_REGION),$(AWS_PUBLISH_REGION))
copy_ami_to_publish_region:
	@echo "Build and publish regions are the same - nothing to do"
else
copy_ami_to_publish_region: AMI_ID ?= $(shell $(jq_manifest_build_ami_id))
copy_ami_to_publish_region: AMI_NAME ?= unknown
copy_ami_to_publish_region: manifest_new := $(shell mktemp)
copy_ami_to_publish_region: .check_credentials
	@echo "Copying AMI from $(AWS_BUILD_REGION) to $(AWS_PUBLISH_REGION)"
	aws --region "$(AWS_PUBLISH_REGION)" ec2 copy-image \
		--name "$(AMI_NAME)" \
		--source-region "$(AWS_BUILD_REGION)" \
		--source-image-id "$(AMI_ID)" \
		--copy-image-tags | jq -r '.ImageId' > "$(manifest_new)"
	@echo "Publish region AMI ID: $$(cat "$(manifest_new)")"
	@jq --arg image_id "$$(cat "$(manifest_new)")" \
		--arg region "$(AWS_PUBLISH_REGION)" \
		'.builds[-1].artifact_id += ",\($$region):\($$image_id)"' \
		"$(MANIFEST_FILE)" > "$(manifest_new)"
	@mv "$(manifest_new)" "$(MANIFEST_FILE)"
endif

.PHONY: packer-publish
packer-publish: AMI_ID ?= $(shell $(jq_manifest_publish_ami_id))
packer-publish: .check_credentials
	aws --region "$(AWS_PUBLISH_REGION)" --output json \
		ec2 modify-image-attribute \
		--image-id "$(AMI_ID)" \
		--launch-permission "Add=[{UserId=$(PUBLISH_ACCOUNT_ID)}]"

.PHONY: delete-images
delete-images: AWS_REGION ?= $(AWS_BUILD_REGION)
delete-images: images_list_file := $(shell mktemp)
delete-images: .check_credentials
	@aws --region "$(AWS_REGION)" \
		--output json ec2 describe-images \
		--owners "self" \
		--filters 'Name=tag:Version,Values=$(VERSION)' | jq -r '.Images[] | "\(.ImageId)"' > "$(images_list_file)"
	@if [ ! -s "$(images_list_file)" ]; then \
		echo "No AMIs to delete"; \
	else \
		@echo "AMIS_TO_DELETE='$$(xargs < "$(images_list_file)")'"; \
		@AWS_REGION=$(AWS_REGION) AMIS_TO_DELETE="$$(xargs < "$(images_list_file)")" ./cleanup_amis.sh; \
	fi

.PHONY: delete-stale-images
delete-stale-images: images_list_file := $(shell mktemp)
delete-stale-images: days_ago_30 := $(shell date -u -d "@$$(($(shell date +%s) - 30*24*60*60))" "+%Y-%m-%dT%H:%M:%SZ")
delete-stale-images: .check_credentials
	@echo "Querying for AMIs in $(AWS_REGION) created before $(days_ago_30)"
	@aws --region "$(AWS_REGION)" \
		ec2 describe-images \
		--owners "self" \
		--filters "Name=tag:Family,Values=macos-*" "Name=tag:GitlabProjectPath,Values=$(CI_PROJECT_PATH)" \
		--query 'Images[?CreationDate<`'$(days_ago_30)'` && Tags[?Key==`GitlabCommitRefName` && Value!=`main`]].{ImageId:ImageId}' \
		--output text > "$(images_list_file)"
	@if [ ! -s "$(images_list_file)" ]; then \
		echo "No AMIs to delete"; \
	else \
		@echo "AMIS_TO_DELETE='$$(xargs < "$(images_list_file)")'"; \
		@AWS_REGION=$(AWS_REGION) AMIS_TO_DELETE="$$(xargs < "$(images_list_file)")" ./cleanup_amis.sh; \
	fi

.PHONY: delete-hardcoded-images
delete-hardcoded-images: .check_credentials
	@AWS_REGION=$(AWS_REGION) AMIS_TO_DELETE="$(AMIS_TO_DELETE)" ./cleanup_amis.sh

.PHONY: .check_credentials
.check_credentials:
ifeq ($(AWS_ACCESS_KEY_ID), unknown)
	@echo "AWS_ACCESS_KEY_ID has to be defined"
	@exit 1
endif
ifeq ($(AWS_SECRET_ACCESS_KEY), unknown)
	@echo "AWS_SECRET_ACCESS_KEY has to be defined"
	@exit 1
endif
	@echo "Credentials are set properly"
